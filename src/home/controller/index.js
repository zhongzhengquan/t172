'use strict';

import Base from './base.js';

export default class extends Base {
	init(...args) {
		super.init(...args);
		this.tablePrefix = ""; //将数据表前缀设置为空
		this.tableName = "students"; //将对应的数据表名设置为 classes
	}
}