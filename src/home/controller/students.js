'use strict';

var simpleFetch = require('simple-fetch');
var getJson = simpleFetch.getJson;
var _ = require("underscore");

export default class extends think.controller.rest {
  init(http) {
    super.init(http);
    this._method = "_method";
  }
  async __before() {}
  async getAction() {
    let classes = this.model("classes"),
      page_size = this.get("page_size") || 100,
      page = this.get("page") || 1,
      data = new Array(),
      queryWords = this.get("queryWords");

    let classData = await classes.where({
      class_name: queryWords
    }).find();
    if (!_.isEmpty(classData)) { //直接查询班级下的未学生
      // console.log(1);
      data = await this.modelInstance.limit(page_size).where({
        "clazz_id": classData._id,
        "signature": {
          $exists: false
        }
      }).select();
      // console.log((!_.isEmpty(data)))
      if (!_.isEmpty(data)) {
        data = _.map(data, (val) => {
          return _.extend(val, {
            "class_name": classData.class_name
          });
        });
      }
      console.log(data.length);
    } else { //查询未签名学生
      // console.log(2);
      data = await this.modelInstance.limit(page_size).where({
        "signature": {
          $exists: false
        },
        $or: [{
          student_id: {
            $eq: queryWords
          }
        }, {
          student_name: {
            $eq: queryWords
          }
        }, {
          phone_number: {
            $eq: queryWords
          }
        }, {
          id_number: {
            $eq: queryWords
          }
        }, {
          erp_number: {
            $eq: queryWords
          }
        }]
      }).select();
      console.log(data.length);
      let class_ids = _.map(data, (val) => {
        return val.clazz_id
      });
      let classesColl = await classes.where({
        _id: {
          $in: class_ids
        }
      }).select();
      data = _.map(data, (student) => {
        let className = _.find(classesColl, (val) => {
          return JSON.stringify(val._id) == JSON.stringify(student.clazz_id);
        });
        return _.extend(student, {
          "class_name": className.class_name
        });
      });
      // console.log(data);
    }
    let pages = 1;
    this.header("pages", pages); //设置 header
    // console.log(data);
    this.json(data); //设置返回格式
  }
  async putAction() {
    let pk = await this.modelInstance.getPk();
    console.log(pk);
    let data = this.post();
    let rows = await this.modelInstance.where({
      [pk]: data.id
    }).update({
      "signature": data.signature,
      "signature_date": new Date()
    });
    console.log(rows);
    this.json({
      "status": "done"
    });
  }
  async deleteAction() {
    let pk = await this.modelInstance.getPk();
    let rows = await this.modelInstance.where({
      [pk]: this.id
    }).find();
    let updateRes = await this.modelInstance.where({
      [pk]: this.id
    }).update({
      $unset: {
        "signature": "",
        "signature_date": ""
      },
      $set: {
        signature_bakup: rows.signature || rows.signature_bakup,
        signatureDate_bakup: rows.signature_date || rows.signatureDate_bakup
      }
    });
    console.log(updateRes);
    this.json({
      "status": "done"
    });
  }
  async __call() {
    console.log("__call");
    return this.fail(think.locale("ACTION_INVALID", this.http.action, this.http.url));
  }
}